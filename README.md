# OpenVPN loop wrapper for background connection

This PowerShell script wraps the `openvpn.exe` command and loops if the client fails to start or exits. The command comes with the OpenVPN Community client (<https://openvpn.net/community-downloads/>).

While documents on the web published years ago tend to indicate that the OpenVPN service installed by the client should spawn connection processes for all public client configuration files automatically when it starts, I could not get it working after trying for several hours. Nothing is spawned. Other references I found suggest that we leverage the Windows Task Scheduler to init the connection when the system starts, but those approaches all use the `openvpn-gui.exe` application, which is not a good fit for a background service. Lacking daemon support, the Windows version of `openvpn.exe` itself cannot start as a daemon, so cannot be used in the Task Scheduler directly.

Highlights:

- It uses a `ping` based method to check the Internet connection first before attempting to start the OpenVPN client. The Internet connection check destination is google.com by default, but this is configurable.
- The minimum waiting time between the initiation of two consecutive connection attempts is 120 seconds. The minimum waiting time between the termination of the previous OpenVPN client and the initiation of the next connection attempt is 10 seconds. Both timeouts are configrable.
- It is a wrapper, not a daemon.
- It uses the `openvpn.exe` command, instead of the `openvpn-gui.exe` application. The latter is not designed to run as Local System and may cause conflicts if the user plans to bring up more OpenVPN connections manually.
- It is designed to be wrapped by [NSSM - the Non-Sucking Service Manager](https://nssm.cc/) to create a Windows service that automatically starts and maintains a OpenVPN connection whenever the host is connected to the Internet.

It is common that an OpenVPN server is configured to support persistent connections and such configuration will be pushed to the client upon a successful connection. Therefore, the `openvpn.exe` command does not exit even if it temporarily loses connection to the OpenVPN server. However, if the Internet connection is not ready when the command is invoked, it will exit immediately without trying to reconnect. Hence we have this loop wrapper to specifically address such scenarios.

References:

- <https://www.ovpn.com/en/blog/windows-run-openvpn-automatically-on-computer-startup>
- <https://superuser.com/questions/1166026/how-to-autostart-and-autoconnect-openvpn-in-windows-10> -
- <https://superuser.com/questions/1463227/automatically-connect-to-openvpn-server-on-boot>
